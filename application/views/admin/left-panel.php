<!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="images/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="images/logo2.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li <?php if (@$menu == "Dashboard") {echo "class='active' "; } ?>>
                        <a href="<?= base_url('admin/dashboard')?>"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <li class="menu-item-has-children dropdown <?php if (@$menu == "Manage") {echo "active"; }?>">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Manage</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="<?= base_url('admin/manage/guru')?>">Data Guru</a></li>
                            <li><i class="fa fa-id-badge"></i><a href="ui-badges.html">Data Siswa</a></li>
                            <li><i class="fa fa-bars"></i><a href="ui-tabs.html">Nilai</a></li>
                        </ul>
                    </li> 
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Content</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="ui-buttons.html">Sambutan</a></li>
                            <li><i class="fa fa-id-badge"></i><a href="ui-badges.html">Berita</a></li>
                            <li><i class="fa fa-bars"></i><a href="ui-tabs.html">Tentang</a></li>
                            <li><i class="fa fa-share-square-o"></i><a href="ui-social-buttons.html">Visi & Misi</a></li>
                            <li><i class="fa fa-id-card-o"></i><a href="ui-cards.html">Galeri</a></li>
                        </ul>
                    </li>  
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>Setting</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="ui-buttons.html">Sosial Media</a></li>
                            <li><i class="fa fa-id-badge"></i><a href="ui-badges.html">Akun</a></li>
                            <li><i class="fa fa-bars"></i><a href="ui-tabs.html">Profil Jurusan</a></li>
                        </ul>
                    </li>                 
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->    
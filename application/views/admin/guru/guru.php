<div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="<?= base_url('admin/Dashboard')?>">Dashboard</a></li>
                            <li><a href="#">Table</a></li>
                            <li class="active">Data table</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                  <?php if ($this->session->flashdata('pesan')): ?>
                          <div class="alert alert-success"><?php echo $this->session->flashdata('pesan'); ?></div>
                        <?php endif ?>
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Table</strong>
                            <button type="button" class="btn btn-info pull-right btn-sm" data-toggle="modal" data-target="#myModal" title="Tambah Data Siswa"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="card-body">
                  <table id="bootstrap-data-table" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>NIP</th>
                        <th>Nama Lengkap</th>
                        <th style="width: 50px;">JK</th>
                        <th>Tempat Lahir</th>
                        <th>Tanggal Lahir</th>
                        <th style="width: 50px;">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $i= 1;
                        foreach ($guru->result() as $row) {
                      ?>
                      <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $row->nip;?></td>
                        <td><?= $row->nama_guru;?></td>
                        <td><?= $row->jk; ?></td>
                        <td><?= $row->tempat_lahir; ?></td>
                        <td><?= $row->tanggal_lahir; ?></td>
                        <td>
                          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#edit-data<?= $row->nip;?>" title="Edit Guru"><i class="fa fa-pencil"></i></button>
                          <a href="<?= base_url('admin/manage/deleteGuru/').$row->nip?>" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau di Hapus?')" title="Hapus Carousel"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>            
                      <?php } ?>
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

        <!-- Modal Tambah -->
  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <!-- konten modal-->
      <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
          <h5 class="modal-title" id="mediumModalLabel">Data Guru</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <!-- body modal -->
        <div class="modal-body">
          <form action="<?= base_url('admin/manage/addGuru')?>" class="form-horizontal tasi-form" method="post">
              <div class="row form-group">
                  <label class="col-sm-4 control-label">NIP</label>
                  <div class="col-sm-8">                    
                    <input type="text" class="form-control" name="nip" required="" minlength="18" maxlength="18">
                    <span class="help-block">18 Karakter</span>
                  </div>
              </div>
              
              <div class="row form-group">
                  <label class="col-sm-4 control-label">Nama Lengkap</label>
                  <div class="col-sm-8">                    
                    <input type="text" class="form-control" name="nama" required="">
                  </div>
              </div>

              <div class="row form-group">
                  <label class="col-sm-4 control-label">Jenis Kelamin</label>
                  <div class="col-sm-8">                    
                    <select class="form-control" name="jk" required="">       
                    <option  value="">---Pilih Jenis Kelamin---</option>         
                      <option value="L">Laki-laki</option>
                      <option value="P">Perempuan</option>
                    </select>    
                  </div>
              </div>

              <div class="row form-group">
                  <label class="col-sm-4 control-label">Tempat Lahir</label>
                  <div class="col-sm-8">                    
                    <input type="text" class="form-control" name="tempat_lahir" required="">
                  </div>
              </div>

              <div class="row form-group">
                  <label class="col-sm-4 control-label">Tanggal Lahir</label>
                  <div class="col-sm-8">                    
                    <input type="date" class="form-control" name="tanggal_lahir" required="" >
                  </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
              </div>
          </form>
        </div>        
      </div>
    </div>
  </div>

  <?php 
  foreach ($guru->result() as $row) {
 ?>
 <!-- Modal Ubah Data -->
  <div id="edit-data<?= $row->nip; ?>" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- konten modal-->
      <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h4 class="modal-title">Ubah Data</h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
          <form action="<?= base_url('admin/manage/editGuru')?>" class="form-horizontal tasi-form" method="post">
              <div class="row form-group">
                  <label class="col-sm-4 control-label">NIP</label>
                  <div class="col-sm-8">
                    <input type="hidden" name="nip" value="<?= $row->nip;?>">
                    <input type="text" class="form-control" name="nip" required="" minlength="18" maxlength="18" value="<?= $row->nip; ?>" disabled>                    
                  </div>
              </div>

              <div class="row form-group">
                  <label class="col-sm-4 control-label">Nama Lengkap</label>
                  <div class="col-sm-8">                    
                    <input type="text" class="form-control" name="nama" required="" value="<?= $row->nama_guru; ?>">
                  </div>
              </div>

              <div class="row form-group">
                  <label class="col-sm-4 control-label">Jenis Kelamin</label>
                  <div class="col-sm-8">                    
                    <select class="form-control" name="jk">       
                    <option  value="<?= $row->jk; ?>">--Pilih Jenis Kelamin--</option>         
                      <option value="L-laki">Laki-laki</option>
                      <option value="P">Perempuan</option>
                    </select>    
                  </div>
              </div>

              <div class="row form-group">
                  <label class="col-sm-4 control-label">Tempat Lahir</label>
                  <div class="col-sm-8">                    
                    <input type="text" class="form-control" name="tempat_lahir" required="" value="<?= $row->tempat_lahir; ?>">
                  </div>
              </div>

              <div class="row form-group">
                  <label class="col-sm-4 control-label">Tanggal Lahir</label>
                  <div class="col-sm-8">                    
                    <input type="date" class="form-control" name="tanggal_lahir" required="" value="<?= $row->tanggal_lahir; ?>">
                  </div>
              </div>


              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
              </div>             
          </form>
        </div>        
      </div>
    </div>
  </div>
<?php } ?>
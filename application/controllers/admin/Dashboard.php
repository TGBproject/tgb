<?php if (! defined('BASEPATH')) exit('No direct script acces allowed');
	
	class Dashboard extends CI_Controller{
		function __construct(){
			parent::__construct();
			if(!$this->session->userdata('username')){
			redirect('admin/login');			
			}
		}

		public function index(){
			$data['menu'] 	= 'Dashboard';
			$data['title'] = 'Admin - Dashboard';	
			$data['cont'] = 'admin/dashboard';
			$this->load->view('admin/template',$data);
		}
		
	}
 ?>
<?php if (! defined('BASEPATH')) exit('No direct script acces allowed');
	
	class Manage extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model("admin/m_admin");
			if(!$this->session->userdata('username')){
			redirect('admin/login');			
			}
		}

		public function index(){
			
		}

		public function guru(){
			$data['menu'] = 'Manage';
			$data['guru'] = $this->m_admin->ambil('guru');
			$data['title'] = 'Admin - Manage';	
			$data['cont'] = 'admin/guru/guru';
			$this->load->view('admin/template',$data);
		}

		public function addGuru(){
			$data = array(
				'nip' => $_POST['nip'],
				'nama_guru' => $_POST['nama'],
				'jk' => $_POST['jk'],		
				'tempat_lahir' => $_POST['tempat_lahir'],
				'tanggal_lahir' => $_POST['tanggal_lahir']
			);
			$this->m_admin->tambah('guru',$data);
			$this->session->set_flashdata('pesan','Data Berhasil ditambahkan');
			redirect('admin/manage/guru');
		}

		public function editGuru(){
			$data = array(				
				'nama_guru' => $_POST['nama'],
				'jk' => $_POST['jk'],		
				'tempat_lahir' => $_POST['tempat_lahir'],
				'tanggal_lahir' => $_POST['tanggal_lahir']
			);
			$id['nip'] = $_POST['nip'];
			$this->m_admin->ubah($id, 'guru', $data);
			$this->session->set_flashdata('pesan','Data berhasil diperbaharui');
			redirect('admin/manage/guru');
		}

		public function deleteGuru($id){
			$this->db->delete('guru',array('nip'=> $id));
			$this->session->set_flashdata('pesan','Data berhasil Hapus');
			redirect('admin/manage/guru');
		}
		
	}
 ?>
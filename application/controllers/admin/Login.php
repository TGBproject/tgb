<?php if (! defined('BASEPATH')) exit('No direct script acces allowed');
	
	class Login extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('admin/M_login');
		}

		public function index(){
			$data['title'] = 'Halaman Login Admin';			
			$this->load->view('admin/login',$data);
		}

		public function aksi_login(){
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$cek = $this->M_login->cek($username, md5($password));
			if ($cek->num_rows() == 1) {
				foreach($cek->result() as $data){
					$sess_data['id'] = $data->id;
					$sess_data['username'] = $data->username;					
					$sess_data['nama'] = $data->nama;
					$this->session->set_userdata($sess_data);

			
				}
				redirect('admin/Dashboard');
			}

			else{
				$this->session->set_flashdata('Pesan','Login Gagal');
				redirect('admin/Login');
			}
		}

		public function logout(){
			$this->session->sess_destroy();
			redirect('admin/login');
		}
	}
 ?>
<?php if (! defined('BASEPATH')) exit('No direct script acces allowed');
	
	class M_admin extends CI_Model{
		public function tambah($db, $data){
			$this->db->insert($db, $data);
		}

		public function ambil($db){
			return $this->db->get($db);			
		}

		public function ambil_id($primary ,$id, $db){
			$this->db->where($primary, $id);
			return $this->db->get($db);
		}

		public function ubah($id, $db ,$data){
			$this->db->where($id);
			$this->db->update($db ,$data);
		}

		//ambil data terbaru & batasi 3
		public function ambil_data($primary,$db){
			$this->db->order_by($primary,'DESC');
			$this->db->limit('3');
			$query = $this->db->get($db);
			if ($query->num_rows()>0) {
				return $query->result();
			}else{
				return false;
			}
		}
	}
?>